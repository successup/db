<?php

namespace Successup\DB;

use PDOException;

interface DBInterface
{
	function execParams(string $sql, array $params = []) : int;

	function queryFetchOne(string $sql, array $params = [], /* extra arguments for PDOStatement::fetchAll() */ ...$a) : array;

	function queryFetchOneOrNone(string $sql, array $params = [], /* extra arguments for PDOStatement::fetchAll() */ ...$a) : ?array;

	function queryFetchAll(string $sql, array $param = [], /* extra arguments for PDOStatement::fetchAll() */ ...$a) : array;

	function processPDOException(PDOException $e);

	static
	function e(string $name) : string;

	function requireTransaction();
}
