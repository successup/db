<?php

namespace Successup\DB;

use PDOException;

trait PdoDbCommon
{
	function execParams(string $sql, array $params = []) : int
	{
		try {
			if ($params === [])
				return $this->exec($sql);

			$St = $this->prepare($sql);
			$St->execute($params);
			return $St->rowCount(); }
		catch (PDOException $e) {
			return $this->processPDOException($e); }
	}

	function expectExactlyNRows(int $expected, int $affected)
	{
		if ($expected === $affected)
			return;
		throw new DBUnexpectedRecordCountException(sprintf('expected exactly %d affected rows, got %d', $expected, $affected));
	}

	protected
	function processCode23000(PDOException $e)
	{
		switch ($e->errorInfo[1]) {
		case 1859:	# ER_DUP_UNKNOWN_IN_INDEX, Message: Duplicate entry for key '%s'
		case 1586:	# ER_DUP_ENTRY_WITH_KEY_NAME, Message: Duplicate entry '%s' for key '%s'
		case 1062:	# ER_DUP_ENTRY, Message: Duplicate entry '%s' for key %d
			throw new DBDuplicateEntryException($e);
		default:
			throw $e; }
	}

	function processPDOException(PDOException $e)
	{
		if (($e->getCode() === 23000) || ($e->getCode() === '23000'))
			return $this->processCode23000($e);
		throw $e;
	}

	function queryFetchOne(string $sql, array $params = [], /* extra arguments for PDOStatement::fetchAll() */ ...$a) : array
	{
		if (empty($a))
			$a = [\PDO::FETCH_BOTH];
		$a = $this->queryFetchAll($sql, $params, ...$a);

		switch (count($a)) {
		case 1:
			return array_shift($a);
		case 0:
			throw new DBNotFoundException('expected exactly one record, got none');
		default:
			throw new DBMultipleRecordsException('expected exactly one record, got ' .count($a)); }
	}

	function queryFetchOneOrNone(string $sql, array $params = [], /* extra arguments for PDOStatement::fetchAll() */ ...$a) : ?array
	{
		$a = $this->queryFetchAll($sql, $params, ...$a);

		switch (count($a)) {
		case 1:
			return array_shift($a);
		case 0:
			return null;
		default:
			throw new DBMultipleRecordsException('expected exactly one record, got ' .count($a)); }
	}

	function queryFetchAll(string $sql, array $param = [], /* extra arguments for PDOStatement::fetchAll() */ ...$a) : array
	{
		if ($param === [])
			$St = $this->query($sql);
		else {
			$St = $this->prepare($sql);
			$St->execute($param); }

		return call_user_func_array([ $St, 'fetchAll' ], $a);
	}

	function queryFetchAllTrace(string $sql, array $param = [])
	{
		if ($param === [])
			$St = $this->queryTrace($sql);
		else {
			$St = $this->prepare($sql);
			$St->executeTrace($param); }
	}

	function requireTransaction()
	{
		if (!$this->inTransaction())
			throw new \LogicException('a DB transaction is expected and required, but not provided');
	}
}
