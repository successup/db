<?php

namespace Successup\DB;

trait TauDbCommon
{
	function searchCountFromTauRecord(TauRecord $T) : int
	{
		return $this->queryFetchOne($T->searchCountSql(), $T->searchCountData(), \PDO::FETCH_NUM)[0];
	}

	function searchFromTauRecord(TauRecord $T) : array
	{
		return $this->queryFetchAll($T->searchSql(), $T->searchData());
	}

	function TauRecord(string $table)
	{
		return new TauRecord($this, $table);
	}

	function updateFromTauRecord(TauRecord $T)
	{
		return $this->execParams($T->updateSql(), $T->updateData());
	}

	function insertFromTauRecord(TauRecord $T)
	{
		return $this->execParams($T->insertSql(), $T->insertData());
	}

	function insertOrUpdateFromTauRecord(TauRecord $T)
	{
		return $this->execParams($T->insertOrUpdateSql(), $T->insertOrUpdateData());
	}
}
