<?php

namespace Successup\DB;

class TauSql
{
	protected $sql;

	function __construct($sql)
	{
		$this->sql = $this->bugtracing_inspect($sql);
	}

	function __toString()
	{
		return $this->sql;
	}

	function bugtracing_inspect($sql)
	{
		if (!is_string($sql))
			throw new LogicException('expected string');
		switch ($sql) {
		case 'NOW()':
			return $sql;
		default:
			throw new Exception('unsupported SQL: "' .$sql .'"'); }
	}
}
