<?php

namespace Successup\DB;

use PDOException;

class DBException extends PDOException {}
