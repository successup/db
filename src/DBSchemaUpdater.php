<?php

namespace Successup\DB;

use ArrayAccess;

class DBSchemaUpdater implements ArrayAccess
{
	private $DB;
	private $aa = [];

	function __construct(DBInterface $DB)
	{
		$this->DB = $DB;
	}

	function offsetExists($offset) : bool
	{
		return array_key_exists($offset, $this->aa);
	}

	function offsetGet($offset)
	{
		return $this->aa[$offset];
	}

	function offsetSet($offset, $value) : void
	{
			# to avoid wrong version numbers being passed 'by hand',
			# and leading to mistakes etc.
			# let's just disallow specific version numbers outright
		if ($offset !== null)
			throw new Exception('specific version numbers not supported right now');

		if ($offset === null)
			$this->aa[] = $value;
		else
			$this->aa[$offset] = $value;
		$this->handleUpdateX();
	}

	function offsetUnset($offset)
	{
		unset($this->aa[$offset]);
	}

	protected
	function currentVersion() : int
	{
		return ($this->DB->query('PRAGMA user_version'))->fetch()['user_version'];
	}

	protected
	function setCurrentVersion(int $version)
	{
		$this->DB->exec('PRAGMA user_version = ' .((int) $version));
	}

	protected
	function latestMigrationFor() : int
	{
		return count($this->aa) - 1;
	}

	protected
	function performLatestMigration()
	{
		$sql = $this->aa[$this->latestMigrationFor()];
		$this->DB->exec($sql);
	}

	protected
	function handleUpdateX() : void
	{
		if ($this->latestMigrationFor() === $this->currentVersion()) {
			$this->performLatestMigration();
			$this->setCurrentVersion($this->currentVersion() + 1); }
	}
}
