<?php

namespace Successup\DB;

class DBDuplicateEntryException extends DBIntegrityConstraintViolationException
{
	function __construct($e)
	{
		$this->errorInfo = $e->errorInfo;
		$this->code = $e->code;

		$this->message = $e->message;
	}
}
