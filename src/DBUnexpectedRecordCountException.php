<?php

namespace Successup\DB;

class DBUnexpectedRecordCountException extends DBException {}
