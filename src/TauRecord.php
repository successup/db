<?php

namespace Successup\DB;

class TauRecord
{
	protected $DB;
	protected $table;
	protected $andCond = [];
	protected $limit = null;
	protected $offset = null;
	protected $condData = [];
	protected $extraPrefix = null;
	protected $orderBy = [];

	protected $data = [];

	function __construct(DBInterface $DB, string $table)
	{
		$this->DB = $DB;
		$this->table = $table;
	}

	function E(string $name) : string
	{
		return $this->DB->e($name);
	}

	function updateSql() : string
	{
		$ret = 'UPDATE ' .$this->E($this->table) .' SET ';
		$a = [];

		foreach ($this->data as $k => $v) {
			if ($v instanceof TauSql)
				$a[] = ' ' .$this->E($k) .' = ' .$v;
			else
				$a[] = ' ' .$this->E($k) .' = ?';
		}

		$ret .= implode(', ', $a);

		$ret .= ' WHERE ' .$this->condSql();

		return $ret;
	}

	function searchCountSql() : string
	{
		$ret = 'SELECT';
		$o = [];

		if ($this->extraPrefix !== null)
			$ret .= ' ' .$this->extraPrefix .' ';

		$ret .= ' COUNT(*)';

		$ret .= '
			FROM ' .$this->E($this->table) .'
			WHERE ' .$this->condSql();

		if (!empty($this->orderBy))
			$ret .= ' ORDER BY ' .implode(', ', array_map(function($rcd) {
				if ($rcd[0] instanceof TauRecord)
					return $rcd[0] .' ' .$rcd[1];
				else
					return $this->E($rcd[0]) .' ' .$rcd[1];
			}, $this->orderBy));

		return $ret;
	}

	function searchSql() : string
	{
		$ret = 'SELECT ';
		$a = []; $o = [];

		if ($this->extraPrefix !== null)
			$ret .= ' ' .$this->extraPrefix .' ';

		if (empty($this->data))
			$a[] = '*';
		else {
			foreach ($this->data as $k => $v)
				if ($v instanceof TauSql)
					$a[] = $v .' AS ' .$this->E($k);
				else
					$a[] = $this->E($k); }

		$ret .= implode(', ', $a);

		$ret .= '
			FROM ' .$this->E($this->table) .'
			WHERE ' .$this->condSql();

		if (!empty($this->orderBy))
			$ret .= ' ORDER BY ' .implode(', ', array_map(function($rcd) {
				if ($rcd[0] instanceof TauRecord)
					return $rcd[0] .' ' .$rcd[1];
				else
					return $this->E($rcd[0]) .' ' .$rcd[1];
			}, $this->orderBy));

			# non-NULL
		if (isset($this->limit))
			$ret .= ' LIMIT ?';
		if (isset($this->offset))
			$ret .= ' OFFSET ?';

		return $ret;
	}

	function orderBy($columnOrTauSql, $direction = 'ASC')
	{
		$supportedDirections = [ 'ASC', 'DESC' ];
		if (in_array($direction, $supportedDirections, $strict = true))
			;
		else
			throw new Exception(sprintf('unsupported direction "%s"', $direction));

		$this->orderBy[] = [ $columnOrTauSql, $direction ];
	}

	function insertSql() : string
	{
		$a = [];
		$a[] = 'INSERT INTO ' .$this->E($this->table);

		$a[] = '(';

		$a[] = implode(', ',
			array_map(
				[ $this, 'E' ],
				array_keys($this->data) ) );

		$a[] = ') VALUES (';

		$a[] = implode(', ',
			array_map(
				function($v) : string
				{
					if ($v instanceof TauSql)
						return $v;
					else
						return '?';
				},
				array_values($this->data) ) );

		$a[] = ')';

		return implode(' ', $a);
	}

	function insertOrUpdateSql()
	{
		$this->condSql();	# calc the $this->condData

		$a = [];
		$a[] = 'INSERT INTO ' .$this->E($this->table);

		$a[] = '(';

		$a[] = implode(', ',
			array_merge(
				array_map(
					[ $this, 'E' ],
					array_keys($this->data) ),
				array_map(
					[ $this, 'E' ],
					array_keys($this->condData) ) ) );

		$a[] = ') VALUES (';

		$a[] = implode(', ',
			array_merge(
				array_map(
					function($v) : string
					{
						if ($v instanceof TauSql)
							return $v;
						else
							return '?';
					},
					array_values($this->data) ),
				array_map(
					function($v) : string
					{
						if ($v instanceof TauSql)
							return $v;
						else
							return '?';
					},
				array_values($this->condData) ) ) );

		$a[] = ')';

		$a[] = ' ON DUPLICATE KEY UPDATE ';

		$aa = [];
		foreach ($this->data as $k => $v)
			if ($v instanceof TauSql)
				$aa[] = ' ' .$this->E($k) .' = ' .$v;
			else
				$aa[] = ' ' .$this->E($k) .' = ?';
		$a[] = implode(',', $aa);

		return implode(' ', $a);
	}

	function insertOrUpdateData() : array
	{
		$ret = [];

		foreach ($this->data as $v)
			if ($v instanceof TauSql)
				;
			else
				$ret[] = $v;

		foreach ($this->condData as $v)
			if ($v instanceof TauSql)
				;
			else
				$ret[] = $v;

		foreach ($this->data as $v)
			if ($v instanceof TauSql)
				;
			else
				$ret[] = $v;

		return $ret;
	}

	function insertData() : array
	{
		$ret = [];

		foreach ($this->data as $v)
			if ($v instanceof TauSql)
				;
			else
				$ret[] = $v;

		if ($this->condData)
			throw new Exception('INSERT cannot handle ->condData');

		return $ret;
	}

	function whereMatchAll()
	{
		$this->andCond = 'MATCH-ALL';
	}

	function condSql()
	{
		$this->condData = [];

		if (empty($this->andCond))
			throw new LogicException('got no cond!');
		else if ($this->andCond === 'MATCH-ALL')
			return ' 1 = 1';

		$a = [];

		foreach ($this->andCond as list($col, $op, $val)) {
			if ($val instanceof TauSql)
				$a[] = ' ' .$this->E($col) .' ' .$op .' ' .$val;
			else {
				$a[] = ' ' .$this->E($col) .' ' .$op .' ?';
				$this->condData[$col] = $val; } }

		return implode(' AND ', $a);
	}

	function whereAnd($col, $op, $val)
	{
		$supported_op = ['=', 'LIKE', '!='];
		if (!in_array($op, $supported_op, $strict = true))
			throw new LogicException(sprintf('unsupported op "%s"', $op));
		$this->andCond[] = [$col, $op, $val];
	}

		# where 'NULL' means "no limit / offset"
	function limitOffset(int $limit, int $offset)
	{
		$this->limit = $limit;
		$this->offset = $offset;
	}

	function limit(int $limit)
	{
		$this->limit = $limit;
	}

	function offset(int $offset)
	{
		$this->offset = $offset;
	}

	function updateData()
	{
		$ret = [];

		foreach ($this->data as $v)
			if ($v instanceof TauSql)
				;
			else
				$ret[] = $v;

		foreach ($this->condData as $v)
			if ($v instanceof TauSql)
				;
			else
				$ret[] = $v;

		return $ret;
	}

	function searchCountData() : array
	{
		$ret = [];

		foreach ($this->condData as $v)
			if ($v instanceof TauSql)
				;
			else
				$ret[] = $v;

		return $ret;
	}

	function searchData()
	{
		$ret = [];

		foreach ($this->data as $v)
			if ($v instanceof TauSql)
				;
			else
				$ret[] = $v;

		foreach ($this->condData as $v)
			if ($v instanceof TauSql)
				;
			else
				$ret[] = $v;

		if (isset($this->limit))
			$ret[] = $this->limit;

		if (isset($this->offset))
			$ret[] = $this->offset;

		return $ret;
	}

	function __set($key, $value)
	{
		$this->data[$key] = $value;
	}

	function &__get($key)
	{
		return $this->data[$key];
	}

	function __isset($key)
	{
		return array_key_exists($key, $this->data);
	}

	function rawGenerator()
	{
		return function($sql)
		{
			return new TauSql($sql);
		};
	}

	function array_merge(array $newData)
	{
		$this->data = array_merge($this->data, $newData);
	}
}
