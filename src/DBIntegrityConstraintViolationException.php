<?php

namespace Successup\DB;

class DBIntegrityConstraintViolationException extends DBException {}
