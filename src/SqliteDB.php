<?php

namespace Successup\DB;

use PDO;

abstract
class SqliteDB extends PDO implements DBInterface
{
	use PdoDbCommon;
	use TauDbCommon;

	function __construct()
	{
		$dsn = sprintf('sqlite:%s', static::dbFilePathname());
		$username = null;
		$passwd = null;
		$opt = [
#			PDO::ATTR_STATEMENT_CLASS => [ 'DBStatement', [ $this ] ],
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_EMULATE_PREPARES => false,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC ];
		parent::__construct($dsn, $username, $passwd, $opt);

		$this->setupJournal();

		{{{
			$this->onSchemaVersion = new DBSchemaUpdater($this);

			$this->initIfNeeded();

				# break the circular reference,
				# so that this PDO gets GC'd as soon as we aren't using it anymore
				# thus dis-connecting from the DB
			unset($this->onSchemaVersion);
		}}}
	}

	protected
	function setupJournal()
	{
		$this->setupJournalWal();
	}

		# general use case
	protected
	function setupJournalWal()
	{
		$this->exec('PRAGMA journal_mode = WAL');
	}

		# for the one-shot audit logging
	protected
	function setupJournalPersist()
	{
		$this->exec('PRAGMA journal_mode = PERSIST');
	}

	protected
	function setupJournalTruncate()
	{
		$this->exec('PRAGMA journal_mode = TRUNCATE');
	}

	abstract protected
	function initIfNeeded();

	static
	function dbFilePathname() : string
	{
		return INSTALL_DIR .'/var/lib/sqlite/' .static::dbName() .'.sqlite';
	}

	abstract static
	function dbName() : string;

	static
	function factory() : self
	{
		return new static();
	}

	static
	function e(string $name) : string
	{
		$Q = '"';

		return $Q
			.str_replace($Q, $Q .$Q, $name)
			.$Q;
	}

	function sqliteAttachDatabase(SqliteDB $DB, string $name)
	{
		$this->execParams('ATTACH ? AS ?', [ $DB->dbFilePathname(), $name ]);
	}
}
