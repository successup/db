<?php

namespace Successup\DB;

class DBNotFoundException extends DBUnexpectedRecordCountException {}
