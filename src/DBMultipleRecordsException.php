<?php

namespace Successup\DB;

class DBMultipleRecordsException extends DBUnexpectedRecordCountException {}
